#!/usr/bin/python3

import os
import shutil

GIT = ""

def git_cmd(repo_dir:str, cmd:str) -> None:
    old_dir = os.getcwd()
    print (f"*{repo_dir}: git {cmd}")
    try:
        os.chdir(repo_dir)
        os.system(f"{GIT} {cmd}")
    finally:
        os.chdir(old_dir)

def main():
    global GIT

    print ("Hello")
    if not os.getcwd().endswith("/submod-uids"):
        print ("Run in bad dir", os.getcwd())
        return

    GIT = os.path.abspath("../../git")

    os.system(f"{GIT} --version")

    os.system(f"{GIT} config --global --unset-all core.submoduleuidhook")
    os.system(f"{GIT} config --global core.submoduleuidhook \"{os.path.abspath('my_hook.py')} uidHook\"")
    os.system(f"{GIT} config --global -l")

    shutil.rmtree("tmp", ignore_errors=True)
    os.mkdir("tmp")

    print ("-"*60)

    git_cmd("tmp", "init sub1")
    git_cmd("tmp", "init sub2")
    git_cmd("tmp", "init main1")

    open("tmp/sub1/a", "w").write("File A\n")
    open("tmp/sub2/b", "w").write("File B\n")
    open("tmp/main1/c", "w").write("File C\n")

    git_cmd("tmp/sub1", "add .")
    git_cmd("tmp/sub2", "add .")
    git_cmd("tmp/main1", "add .")
    git_cmd("tmp/sub1", "commit -am wip1")
    git_cmd("tmp/sub2", "commit -am wip1")
    git_cmd("tmp/main1", "commit -am wip1")

    print ("-"*60)

    git_cmd("tmp/main1", "submodule add ../sub1 sub1a")
    git_cmd("tmp/main1", "submodule add ../sub2 sub2a")
    git_cmd("tmp/main1", "commit -am wip2")

    print ("-"*60)

    git_cmd("tmp/main1", "submodule add uid:uid_sub1 sub1b")
    git_cmd("tmp/main1", "submodule add uid:uid_sub2 sub2b")
    git_cmd("tmp/main1", "log")
    git_cmd("tmp/main1", "commit -am wip3")

    print ("-"*60)

    git_cmd("tmp/main1", "submodule add ../uid_sub1.git sub1c")
    git_cmd("tmp/main1", "submodule add ../uid_sub2.git sub2c")
    git_cmd("tmp/main1", "commit -am wip4")

    print ("-"*60)

    git_cmd("tmp", "clone main1 main2")
    git_cmd("tmp/main2", "submodule update --init --recursive")

    print ("-"*60)
    print (open("tmp/main1/.gitmodules","rt").read())
    print (open("tmp/main1/.git/config","rt").read())
    print ("-"*60)

main()
