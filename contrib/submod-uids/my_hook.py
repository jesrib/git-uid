#!/usr/bin/python3

import os
import sys

def main():
    # print ("cwd:", os.getcwd(), file=sys.stderr)

    if sys.argv[1]!="uidHook":
        print ("ERROR:", str(sys.argv), file=sys.stderr)
        return -100
    cmd = sys.argv[2]
    if cmd=="resolve_subm_from_relative_path":
        remoteurl, sub_relative_url, ws_path, up_path = sys.argv[3:]  # remoteurl points to the containing/owning repo "origin", or local path if no remote url.
        url = sub_relative_url.split("/")[-1]
        if url.endswith(".git"):
            url = url[:-4]
        if url=="uid_sub1":
            print ("../sub1")       # always return a relative path. For submodule add, this path is used in .gitmodules too
            print (url)             # TODO: return the UID
            return 0
        if url=="uid_sub2":
            print ("../sub2")       # always return a relative path. For submodule add, this path is used in .gitmodules too
            print (url)             # TODO: return the UID
            return 0
        # couldn't find it:
        return -1
        #print ("TODO:", remoteurl, ":", url, ":", up_path, ":", file=sys.stderr)
    elif cmd=="resolve_subm_from_uid":
        remoteurl, sub_relative_url, ws_path, ws_path, uid = sys.argv[3:]
        if uid=="uid_sub1":
            print ("../sub1")       # always return a relative path. For submodule add, this path is used in .gitmodules too
            print (uid)
            return 0
        if uid=="uid_sub2":
            print ("../sub2")
            print (uid)
            return 0
        print ("ERROR: unknown UID:", remoteurl, a,b,uid, file=sys.stderr)
        return -1
    else:
        print ("Unknown args:", sys.argv, file=sys.stderr)
        return -1

sys.exit(main())
